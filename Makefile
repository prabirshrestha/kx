-include ./Makefile.config

all: boot

boot: bootx86

bootx86:
	cd src/boot/x86 && $(MAKE)

clean:
	cd src/boot/x86 && $(MAKE) $@

.PHONY: boot bootx86
