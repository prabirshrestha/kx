# references:
#   http://www.freebsd.org/doc/en/books/arch-handbook/book.html
#   http://www.websofia.com/2011/10/writing-your-own-boot-loader-for-a-toy-operating-system-1/
#   http://pastebin.com/HHcqnAuT

# During boot, certain PC registers are set to a particurlar value.
# instruction pointer (ip) register is set to 32-bit value of 0xfffffff0
#   ip register points to the code to be executed.
# cr0 register set to 0
#   it is a 32-bit control register
#   one of cr0's bit indicates whether the processor is runing in real or protected mode.
#   during boot time this bit is cleared so it always boots in real mode.
# value of 0xfffffff0 is slightly less then 4gb.

# BIOS and its chip on motherboard has a small amount of ROM which contains
# various low-level routines that are specific to the hardware suplied with motherboard.
# So, the processor will first jump to the address 0xfffffff0,
# which really resides in the BIOS's memory.
# Usually this address contains a jump instruction to the BIOS's POST routines.

# POST stands for Power On Self Test.
# This is a set of routines including the
#   memory check,
#   system bus check
#   and other low-level stuff
# so that the CPU can initialize the computer properly.
# The important step on this stage is determining the boot device.
# All modern BIOS's allow the boot device to be set manually, so you can boot from a floppy, CD-ROM, harddisk etc.

# The very last thing in the POST is the INT 0x19 instruction
# That instruction reads 512 bytes from the first sector of boot device into the memory at address 0x7c00
# This is first sector is also called MBR or Master Boot Record

.code16                     # 16 bit code generation
                            # Since all intel based start up with 16-bit instructions,
                            # we won't be able to write 32 bit code yet. 

.intel_syntax noprefix      # instruct GNU assembler that we’ll be using Intel syntax
                            # (e.g. mov ax, 1 instead of movw $1, %ax – some prefer the latter

.text                       # code segment
.org 0x0                    # the origin of our code will be 0×0,
                            # i.e. all absolute addresses start at 0×0, which will be convenient.

# Constants
.set STAGE2_SEGMENT, 0x0000     # stage 2 load segment
.set STAGE2_OFFSET, 0x1000 - 24 # stage 2 load offset
.set STAGE2_BYTES, 0x8000       # 32768 bytes / 32 kiB.
.set STAGE2_SECTORS, 54         # Stage 2 sectors (CHS HDDs)

.global main                # making entry point visible to the linker

main:
    jmp stage1_start        # this is the main entry point of our code which corresponds to the first byte
    nop                     # of actual output when assembled The code under "main" simply jumps over the
                            # BPB and EBPB located at offset 0×3, resuming execution at the label stage1_start. 



# code that prints a "Press any key to reboot" message, waits for a keystroke, and reboots the machine.
.func Reboot
 Reboot:
    lea    si, rebootmsg # Load address of reboot message into si
    call   WriteString   # print the string
    xor    ax, ax        # subfuction 0
    int    0x16          # call bios to wait for key
    .byte  0xEA          # machine language to jump to FFFF:0000 (reboot)
    .word  0x0000
    .word  0xFFFF
.endfunc

# Printing on screen
# function that uses BIOS interrupt 0×10, sub-function 9 to print characters to the screen
# The calling code must point DS:SI to the null-terminated string to be printed
.func WriteString
 WriteString:
    lodsb                   # load byte at ds:si into al (advancing si)
    or     al, al           # test if character is 0 (end)
    jz     WriteString_done # jump to end if 0.
    mov    ah, 0xe          # Subfunction 0xe of int 10h (video teletype output)
    mov    bx, 9            # Set bh (page nr) to 0, and bl (attribute) to white (9)
    int    0x10             # call BIOS interrupt
    jmp    WriteString      # Repeat for next character

 WriteString_done:
    retw
.endfunc

# Write a string to stdout.
# str = string to write
.macro mWriteString str
  lea    si,    \str
  call   WriteString
.endm

# Reset disk system.
# This is done through interrupt 0x13, subfunction 0.
# If reset fails, carry is set and we jump to reboot.
.macro mResetFddSystem
  mov  dl, iBootDrive    # drive to reset
  xor  ax, ax            # subfunction 0
  int  0x13              # call interrupt 13h
  # jc   bootFailure       # display error message if carry set (error)  
.endm

stage1_start:
# Initialize various segments:
# - The boot sector is loaded by BIOS at 0:7C00 (CS = 0)
# - CS, DS and ES are set to zero.
# - Stack point is set to 0x7C00 and will grow down from there.
# - Interrupts may not act now, so they are disabled.
    cli                     # block all maskable interrupts (does not block NMI - non-maskable interrupts)
    mov  ax, cs             # CS is set to 0x0, because that is where boot sector is loaded (0:07c00)
    mov  ds, ax             # DS = CS = 0x0 
    mov  es, ax             # ES = CS = 0x0
    mov  ss, ax             # SS = CS = 0x0
    mov  sp, 0x7C00         # Stack grows down from offset 0x7C00 toward 0x0000.
    mov  iBootDrive, dl     # save what drive we booted from (should be 0x0)
                            # BIOS places the number of the boot drive in the DL register. We store it in our BPB for later use.
    sti                     # enable interrupts
#end init segements

# Decide whether we are booting from hard disk (HDD) or floppy (FDD).
is_hdd_or_fdd:
    cmp byte ptr[iBootDrive], 0x0 # fdd(0x0), hdd(0x80)
    je read_fdd
    mWriteString hdd
    jmp hang

read_fdd:
  # display A
  mov ah,0x0E 
  mov bh,0x00 
  mov al,65
  int 0x10

  mResetFddSystem

  # Set ES:BX to STAGE2_SEGMENT:STAGE2_OFFSET
  mov     bx,     STAGE2_SEGMENT
  mov     es,     bx
  mov     bx,     STAGE2_OFFSET
  
  mov ax,0x1000 # When we read the sector, we are going to read address 0x1000
  mov es,ax   #Set ES with 0x1000

# Try to read the sectors.
read_floppy_sectors:
    mov     ah,     0x02            # Read sectors.
    mov     al,     STAGE2_SECTORS  # Sectors 0 .. 63.
    mov     ch,     0               # Cylinder 0.
    mov     cl,     2               # Sector 2.
    mov     dh,     0               # Head 0.
    mov     dl,     [iBootDrive]    # Use the BIOS device.
    int     0x13
    jc read_floppy_sectors # If carry flag was set, try again (todo: retry)

start_stage_1_5:
  jmp STAGE2_OFFSET:STAGE2_SEGMENT # Jump to the start of second program

# using combination of cli/hlt/jmp can create better efficient power saving
# infinite loops then just using jmp
hang:
    cli                     # block all maskable interrupts (does not block NMI - non-maskable interrupts)
    .hang:
        hlt                 # halts until an interrup occurs
        jmp .hang           # infinite loop

bootFailure:
  lea  si, diskerror
  call WriteString
  call Reboot

# Data Section
iBootDrive:     .byte   0   # holds drive that the boot sector came from
fdd:  .asciz "fdd"
hdd:  .asciz "hdd"
diskerror:  .asciz "Disk error"
rebootmsg:  .asciz "Press any key to reboot"
r:  .asciz "r"

.fill (510-(.-main)), 1, 0  # Pad with nulls up to 510 bytes (exclude boot magic)

BootMagic:  .int 0xAA55     # magic word for BIOS
                            # if the last word of bootsector contains 0xAA55,
                            # then this disk is treated by BIOS as bootable
