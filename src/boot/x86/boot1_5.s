.code16                     # 16 bit code generation
                            # Since all intel based start up with 16-bit instructions,
                            # we won't be able to write 32 bit code yet. 

.intel_syntax noprefix      # instruct GNU assembler that we’ll be using Intel syntax
                            # (e.g. mov ax, 1 instead of movw $1, %ax – some prefer the latter

.text                       # code segment
.org 0x0                    # the origin of our code will be 0×0,
                            # i.e. all absolute addresses start at 0×0, which will be convenient.

.global start, _start               # making entry point visible to the linker

start:
_start:
    jmp stage1_5

stage1_5:
# .fill (512-(.-stage1_5)), 1, 0  # Pad with nulls up to 510 bytes (exclude boot magic)

  # display B
  mov ah,0x0E 
  mov bh,0x00 
  mov al,66
  int 0x10

  call bmain